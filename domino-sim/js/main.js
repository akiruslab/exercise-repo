import { Player } from './models/player.js';
import { Tile } from './models/tile.js';
import {
  drawTiles,
  handleWin,
  shuffleTiles,
  print,
  renderTiles,
} from './utility.js';

const renderStyle = {
  GAME: "game",
  MOVE: "move",
  FAIL: "fail",
  END: "end"
}

/*Fields*/
var players = [];
var currentPlayerIndex;
var amountOfTiles; //Could be set to 55, 91, 136 or 190
var tileStock = [];

/*Game fields*/
var isGameOver;
var board = [];
var searchTile;
var isSuccess;
var numSkippedTurns;

/*Main game logic and is called when Play button is clicked*/
document.getElementById('btnPlay').addEventListener('click', function runGame() {
  currentPlayerIndex = 0;
  isGameOver = false;
  isSuccess = false;
  amountOfTiles = 28;
  numSkippedTurns = 0;
  board = [];
  players = [];
  document.getElementById('outputElement').innerHTML = '';
  initTiles();

  print('Game in now starting starting...', renderStyle.GAME);

  //Creating players
  initPlayers(2, 7);
  print('All players(' + players.length + ') have drawn their tiles. ', renderStyle.GAME);

  //Play first tile
  var tile = players[0].tiles.pop();
  board.push(tile);
  print(players[0].name + ' starts the game with ' + tile.render(), renderStyle.MOVE);

  selectNextPlayer();

  while (!isGameOver) {
    //Place tiles until a winner is found
    placeTiles(players[currentPlayerIndex]);
  }
  print('Game Over!', renderStyle.END);
});

/*Method for handling the placement of tiles on the board.
  
   param a Player object representing the player who is taking
          turn
  */
function placeTiles(currentPlayer) {
  for (var index = 0; index < currentPlayer.tiles.length; index++) {
    searchTile = currentPlayer.tiles[index];
    isSuccess = tryPlaceTile(searchTile, currentPlayer);
    if (isSuccess) {
      currentPlayer.removePlayerTile(index); //Remove successfully placed tile
      break;
    }
  }

  if (!isSuccess) {
    if (!(numSkippedTurns >= 2)) {
      handleFailedAttempt(currentPlayer);
      return;
    } else {
      print('None of the players can place tiles on the board', renderStyle.END);
      players.forEach((p) => {
        print(p.name + ' was left with ' + renderTiles(p.tiles), renderStyle.GAME);
      });
      isGameOver = true;
      return;
    }
  }

  print(currentPlayer.name + ' has played ' + searchTile.render(), renderStyle.MOVE);

  print('<<< Board: ' + renderTiles(board) + ' >>>', renderStyle.GAME);

  if (currentPlayer.tiles.length == 0) {
    isGameOver = true;
    handleWin(currentPlayer);
  }

  selectNextPlayer();
}

/*Function that attempts to place a tile. 
  
   returns a boolean value of whether the attempt 
          was successful or not
  */
function tryPlaceTile(searchTile) {
  if (searchTile.rightSide == board[0].leftSide) {
    board.reverse();
    board.push(searchTile);
    board.reverse();

    return true;
  } else if (searchTile.rightSide == board[board.length - 1].rightSide) {
    searchTile.rotateTile();
    board.push(searchTile);

    return true;
  } else if (searchTile.leftSide == board[board.length - 1].rightSide) {
    board.push(searchTile);

    return true;
  } else if (searchTile.leftSide == board[0].leftSide) {
    searchTile.rotateTile();
    board.reverse();
    board.push(searchTile);
    board.reverse();

    return true;
  }
  return false;
}

/*Method for handling a failed attempt at placing a tile on the board.
  
  param a Player object representing the player who is taking
         turn
  */
function handleFailedAttempt(currentPlayer) {
  print(currentPlayer.name + ' could not place a tile and must draw ', renderStyle.FAIL);
  let numOfDraws = 0;

  while (!isSuccess) {
    //Try place tiles until successful or until they draw all of them from the stock
    if (tileStock.length > 0) {
      searchTile = tileStock.pop();

      numOfDraws++;

      isSuccess = tryPlaceTile(searchTile, currentPlayer);

      if (!isSuccess) {
        //Add drawn tile to player's set if it cannot be placed on the board
        currentPlayer.tiles.push(searchTile);
      }
    } else {
      print(
        currentPlayer.name +
        ' could not draw more tiles' +
        ((numOfDraws != 0) ? ', after drawing ' + numOfDraws + ', ': ' ') +
          'and skipped their turn (no more stock)',
        renderStyle.END
      );
      numSkippedTurns++;
      selectNextPlayer();
      return;
    }
  }

  numSkippedTurns = 0; //Resetting skipped turns when a failed attempt is successful

  print(currentPlayer.name + ' drew ' + numOfDraws +
    ((numOfDraws > 1) ? ' tiles ' : ' tile ') +
    'before playing: ' +
    searchTile.render(), renderStyle.MOVE);
  selectNextPlayer();
  print('<<< Board: ' + renderTiles(board) + ' >>>', renderStyle.GAME);
}

/*Method for creating the dominos tile stock depending on the size specified*/
function initTiles() {
  tileStock = [];
  let newTile;
  let leftIndex;
  for (var index = 0; tileStock.length < amountOfTiles; index++) {
    leftIndex = 0;
    while (leftIndex <= index) {
      newTile = new Tile(leftIndex, index);
      tileStock.push(newTile);
      leftIndex++;
    }
  }

  tileStock = shuffleTiles(tileStock);
}

function initPlayers(numOfPlayers, numTilesPerPlayer) {
  for (let i = 1; i <= numOfPlayers; i++) {
    players.push(
      new Player('Player ' + i, drawTiles(numTilesPerPlayer, tileStock))
    );
  }
}

function selectNextPlayer() {
  if (currentPlayerIndex != players.length - 1) {
    currentPlayerIndex++;
  } else {
    currentPlayerIndex = 0;
  }

  return currentPlayerIndex;
}


