/*Method for shuffling the tile stock before the game starts*/
function shuffleTiles(tileStock) {
  for (var i = tileStock.length - 1; i > 0; i--) {
    var j = Math.floor(Math.random() * (i + 1));
    var temp = tileStock[i];
    tileStock[i] = tileStock[j];
    tileStock[j] = temp;
  }

  return tileStock;
}

/*Function for drawing a specified amount tiles from the stock*/
function drawTiles(numberOfTiles, tileStock) {
  var selectedTiles = [];

  for (var index = 0; index < numberOfTiles; index++) {
    selectedTiles.push(tileStock.pop());
  }

  return selectedTiles;
}

function renderTiles(tiles) {
  var output = '';

  tiles.forEach(item => {
    output += item.render();
  });

  return output;
}

function handleWin(winner) {
  print(winner.name + ' has won the game!', "winner");
}

function print(output, styleClass) {
  document.getElementById('outputElement').innerHTML +=
    '<li class="'+ styleClass +'">' + output + '</li>';
}

export {
  shuffleTiles,
  drawTiles,
  renderTiles,
  handleWin,
  print
};
