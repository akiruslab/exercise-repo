class Tile {
  constructor(leftSide, rightSide) {
    this.leftSide = leftSide;
    this.rightSide = rightSide;
  }

  render() {
    return "[" + this.leftSide + " | " + this.rightSide + "]";
  }

  rotateTile() {
    let temp = this.leftSide;
    this.leftSide = this.rightSide;
    this.rightSide = temp;
  }
}

export { Tile };
