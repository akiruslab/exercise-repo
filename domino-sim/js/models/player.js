class Player {
  constructor(name, tiles) {
    this.name = name;
    this.tiles = tiles;
  }

  removePlayerTile(index) {
    this.tiles.splice(index, 1);
    return this.tiles;
  }
}

export { Player };