## Dominos Simulation Project

This project contains the source for a simple dominos simulator written in plain javascript and rendered with HTML.

## Running the Project

1) Navigate to project folder after checking out the code

2) Run php -S localhost:port (replacing port with any available port)

Ensure that you have PHP setup on your machine before attempting to run the project