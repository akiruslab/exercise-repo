﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using UserRepo.Models;

namespace UserRepo.Models
{
    public class UserRepoContext : DbContext
    {
        public UserRepoContext (DbContextOptions<UserRepoContext> options)
            : base(options)
        {
        }

        public DbSet<UserRepo.Models.Users> Users { get; set; }

        public DbSet<UserRepo.Models.ContactDetails> ContactDetails { get; set; }
    }
}
