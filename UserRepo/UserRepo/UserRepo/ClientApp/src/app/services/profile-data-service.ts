import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ProfileModel } from 'src/models/profile';
import { ContactDetailsModel } from 'src/models/contactDetails';
import { Observable } from 'rxjs';

const requestHeaders = new HttpHeaders().set('content-type', 'application/json');

@Injectable()
export class ProfileDataService {
  // Properties

  listOfProfiles: Observable<ProfileModel []>;
  profile: ProfileModel;
  contactDetails: ContactDetailsModel;
  baseUrl;

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  /**
   * Function for creating a new profile in the database.
   *
   * @param profile is a profile model holding new profile data
   */
  createProfile(profile: ProfileModel) {
    const requestBody = {
      UserId: 0,
      FirstName: profile.firstname,
      Surname: profile.surname,
      IdentityNum: profile.identityNum,
      PassportNum: profile.passportNum,
      UserContactDetails: {
        ID: 0,
        MobileNumber: profile.contactDetails.mobileNumber,
        EmailAddress: profile.contactDetails.emailAddress
      }
    };

    const requestUrl = this.baseUrl + 'api/Users';

    const response = this.http.post<ProfileModel>(requestUrl, JSON.stringify(requestBody), {headers: requestHeaders});

    return response;
  }

  /**
   *Function for retreiving all the profiles on the database.
   */
  readProfiles() {
    const requestUrl = this.baseUrl + 'api/Users';

    console.log(requestUrl);

    return this.http.get<ProfileModel[]>(requestUrl);
  }

  /**
   * Function for updating the profile of the given ID in the
   * database.
   *
   * @param profile is a profile model holding updated profile data
   */
  updateProfile(profile: ProfileModel) {
    const requestBody = {
      UserId: profile.id,
      FirstName: profile.firstname,
      Surname: profile.surname,
      IdentityNum: profile.identityNum,
      PassportNum: profile.passportNum,
      UserContactDetails: {
        ID: profile.contactDetails.id,
        MobileNumber: profile.contactDetails.mobileNumber,
        EmailAddress: profile.contactDetails.emailAddress
      }
    };

    const requestUrl = this.baseUrl + 'api/Users/' + profile.id;

    console.log(requestUrl);

    return this.http.put<ProfileModel>(requestUrl, requestBody,
      { headers: requestHeaders});
  }

  /**
   * Function for removing the profile of the given ID from
   * the database.
   *
   * @param profile is a profile model of the profile to be removed
   */
  deleteProfile(userId: number) {
    const requestUrl = this.baseUrl + 'api/Users/' + userId.toString();

    console.log(requestUrl);

    return this.http.delete<ProfileModel>(requestUrl);
  }
}
