import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { ProfileModel } from '../../models/profile';
import { Router } from '@angular/router';
import {NgForm} from '@angular/forms';
import { ProfileDataService } from '../services/profile-data-service';
import { ContactDetailsModel } from '../../models/contactDetails';

@Component({
  selector: 'app-profile-add',
  templateUrl: './profile-add.component.html',
  styleUrls: ['./profile-add.component.css']
})
export class ProfileAddComponent implements OnInit {
  // Properties
  @Input() profileModel: ProfileModel;
  @ViewChild('btnDismissModal') btnClose: ElementRef;
  tempProfile: ProfileModel;

  constructor(private profileService: ProfileDataService, private router: Router) {
    this.profileModel = new ProfileModel(new ContactDetailsModel());
   }

  ngOnInit() {
  }

  submitProfile(addProfileForm: NgForm) {
    this.tempProfile = new ProfileModel(new ContactDetailsModel());
    this.tempProfile.id = 0;
    this.tempProfile.firstname = addProfileForm.value.firstname;
    this.tempProfile.surname = addProfileForm.value.surname;
    this.tempProfile.passportNum = addProfileForm.value.passportNum;
    this.tempProfile.identityNum = addProfileForm.value.identityNum;
    this.tempProfile.contactDetails.mobileNumber = addProfileForm.value.phoneNum;
    this.tempProfile.contactDetails.emailAddress = addProfileForm.value.email;

    this.profileService.createProfile(this.tempProfile).subscribe(resource => {
      addProfileForm.resetForm();
      this.redirect();
    });
  }

  redirect() {
    this.btnClose.nativeElement.click();
    window.location.reload();
  }
}
