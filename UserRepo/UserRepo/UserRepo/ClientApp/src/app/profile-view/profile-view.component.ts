import { Component, OnInit, ViewChild } from '@angular/core';
import { ProfileModel } from '../../models/profile';
import { ProfileEditComponent } from '../profile-edit/profile-edit.component';
import { ContactDetailsModel } from 'src/models/contactDetails';
import { ProfileDataService } from '../services/profile-data-service';

@Component({
  selector: 'app-profile-view',
  templateUrl: './profile-view.component.html',
  styleUrls: ['./profile-view.component.css']
})
export class ProfileViewComponent implements OnInit {
  @ViewChild('editProfileForm') editProfileComponent: ProfileEditComponent;
  profileList: Array<ProfileModel>;
  profile: ProfileModel;
  hasData: Boolean = true;
  isDeleteConfirmed: Boolean = false;

  constructor(private profileService: ProfileDataService) {
    this.profileList = new Array<ProfileModel>();
  }

  ngOnInit() {
    this.profileService.readProfiles().subscribe((profiles) => {
      if(profiles.length > 0) {
        this.extractData(profiles);
      } else {
        this.hasData = false;
      }
    });
  }

  extractData(listOfProfiles) {
    listOfProfiles.forEach(profile => {
      this.profile = new ProfileModel(new ContactDetailsModel());
      this.profile.id = profile.userId;
      this.profile.firstname = profile.firstName;
      this.profile.surname = profile.surname;
      this.profile.identityNum = profile.identityNum;
      this.profile.passportNum = profile.passportNum;
      this.profile.contactDetails.id = profile.userContactDetails.id;
      this.profile.contactDetails.mobileNumber = profile.userContactDetails.mobileNumber;
      this.profile.contactDetails.emailAddress = profile.userContactDetails.emailAddress;

      this.profileList.push(this.profile);
    });
  }

  prepareEditForm(currentProfile: ProfileModel) {
    console.log(currentProfile);
    this.editProfileComponent.profileModel = currentProfile;
  }

  processDelete(userId: number, username: string) {
    this.isDeleteConfirmed = confirm(username + '\'s profile will be deleted. Are you sure you wish to proceed?');

    if(this.isDeleteConfirmed) {
      this.profileService.deleteProfile(userId).subscribe(resource =>{
        window.location.reload();
      });
    }
  }
}
