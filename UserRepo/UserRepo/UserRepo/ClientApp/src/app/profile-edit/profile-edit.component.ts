import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { ProfileModel } from 'src/models/profile';
import { ContactDetailsModel } from 'src/models/contactDetails';
import { ProfileDataService } from '../services/profile-data-service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.css']
})
export class ProfileEditComponent implements OnInit {
  @Output() editEvent = new EventEmitter<string>();
  @ViewChild('btnDismissModal') btnClose: ElementRef;
  @Input() profileModel: ProfileModel;
  constructor(private profileService: ProfileDataService) {
    this.profileModel = new ProfileModel(new ContactDetailsModel());
  }

  ngOnInit() {
  }

  saveProfile(editForm: NgForm) {
    this.profileService.updateProfile(this.profileModel).subscribe(resource => {
      editForm.resetForm();
      this.redirect();
    });
  }

  redirect() {
    this.btnClose.nativeElement.click();
    window.location.reload();
  }

}
