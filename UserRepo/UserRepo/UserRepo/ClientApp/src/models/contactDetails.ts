export class ContactDetailsModel {
  id: number;
  mobileNumber: string;
  emailAddress: string;
}
