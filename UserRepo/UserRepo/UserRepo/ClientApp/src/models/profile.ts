import { ContactDetailsModel } from './contactDetails';

export class ProfileModel {
  id: number;
  firstname: string;
  surname: string;
  identityNum: number;
  passportNum: string;
  contactDetails: ContactDetailsModel;

  constructor(contactDetailsModel: ContactDetailsModel) {
    this.contactDetails = contactDetailsModel;
  }
}
