﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UserRepo.Models
{
    /// <summary>
    /// Model representing a user. The properties for identity and 
    /// passport numbers are specific to users that have a similar
    /// identification to South Africa (numeric ID number and 
    /// alphanumeric passport number).
    /// </summary>
    public class Users
    {
        [Key]
        public int UserId { get; set; }
        public String FirstName { get; set; }
        public String Surname { get; set; }
        public long IdentityNum {get; set; }
        public String PassportNum { get; set; }
        public ContactDetails UserContactDetails{ get; set; }
    }
}
