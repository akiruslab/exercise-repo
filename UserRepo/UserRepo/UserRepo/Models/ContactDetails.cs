﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UserRepo.Models
{
    /// <summary>
    /// Model representing user contact details.
    /// </summary>
    public class ContactDetails
    {
        [Key]
        public int ID { get; set; }
        public String MobileNumber { get; set; }
        public String EmailAddress { get; set; }
    }
}
